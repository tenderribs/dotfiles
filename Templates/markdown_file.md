---
fontfamily: mathpazo
geometry: [a4paper, bindingoffset=0mm, inner=30mm, outer=30mm, top=30mm, bottom=30mm] # See https://ctan.org/pkg/geometry for more options
lang: de-CH
color-links: true # See https://ctan.org/pkg/xcolor for colors
linkcolor: black
urlcolor: black
toc: true
title: SBIC-Chemie Zusammenfassung
author: Mark Marolf
header-includes: |
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyhead[LO,LE]{\today}
    \fancyhead[RO,RE]{Mark Marolf}
    \linespread{1.1}
---

#