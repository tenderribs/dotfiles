# Programs

## Set SSH keys
```sh
ssh-keygen -t ed25519 -C "dc3dude@gmail.com"
cat ~/.ssh/id_ed25519.pub
```

### Bunch of PPAs
```sh
sudo add-apt-repository ppa:aslatter/ppa
sudo add-apt-repository ppa:neovim-ppa/unstable
```

### Vivaldi
```sh
wget -qO- https://repo.vivaldi.com/archive/linux_signing_key.pub | sudo apt-key add -
sudo add-apt-repository 'deb https://repo.vivaldi.com/archive/deb/ stable main'
```


### VSCode
```sh
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
```

```sh
sudo apt install alacritty blender build-essential code composer curl dconf-cli dconf-editor ffmpegthumbnailer flatpak git gnome-tweak-tool gnome-shell-extensions gthumb make neofetch neovim node-typescript pandoc powerline python3 python3-pip python3-venv steam texlive tmux unzip vivaldi-stable wget zsh
```

```sh
chsh -s $(which zsh)
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

reboot

```
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub net.meshlab.MeshLab com.discordapp.Discord com.getpostman.Postman com.prusa3d.PrusaSlicer com.slack.Slack net.meshlab.MeshLab org.freecadweb.FreeCAD org.raspberrypi.rpi-imager org.libreoffice.LibreOffice
```

```sh
# Set location of dotfiles
chezmoi init https://gitlab.com/tenderribs/dotfiles.git
```

```sh
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
# then go into nvim and do a :PlugInstall
```

## Theming

### Pop shell
``` sh
cd ~/Downloads
git clone https://github.com/pop-os/shell.git

cd shell
make local-install # Reset keybindings in prompt
```

### Whitesur
```sh
cd ~/Downloads
git clone https://github.com/vinceliuice/WhiteSur-gtk-theme.git

cd WhiteSur-gtk-theme/
./install.sh
```

### Fonts
careful, download takes forever
```sh
cd ~/Downloads
git clone https://github.com/ryanoasis/nerd-fonts.git

cd nerd-fonts
./install.sh
```
