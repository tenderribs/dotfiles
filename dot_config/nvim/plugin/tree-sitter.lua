require'nvim-treesitter.configs'.setup {
    ensure_installed = maintained, -- Install everything just to make life easy
    indent = { enable = true },
    highlight = { enable = true, additional_vim_regex_highlighting = false},
    incremental_selection = { enable = true },
    textobjects = { enable = true },
    rainbow = { enable = true },
    autotag = { enable = true },
    context_commentstring = { enable = true },
}
