scriptencoding utf-8

call plug#begin('~/.vim/plugged')
" Github help
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'
Plug 'airblade/vim-gitgutter'

Plug 'francoiscabrol/ranger.vim'
Plug 'rbgrouleff/bclose.vim'

" search and replace
Plug 'jremmen/vim-ripgrep'
Plug 'stefandtw/quickfix-reflector.vim'

" Commenting tool
Plug 'https://github.com/scrooloose/nerdcommenter', { 'tag': '*' }

" Markdown
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install' }


" Code autocompletion and Intellisense
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'
Plug 'mattn/emmet-vim'

" Snippets
Plug 'L3MON4D3/LuaSnip'
Plug 'saadparwaiz1/cmp_luasnip'

" Mark indentation lines
Plug 'lukas-reineke/indent-blankline.nvim'

" Neovim Tree shitter
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/playground'

" telescope requirements...
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

" Editor enhancements
Plug 'nvim-lualine/lualine.nvim'

"Colors n shiit
Plug 'gruvbox-community/gruvbox'
call plug#end()

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" SETS and LETS
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if executable('rg')
    let g:rg_derive_root='true'
endif

set hidden
set encoding=utf-8
set number relativenumber
set nu "show line numbers
set nowrap
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set scrolloff=8 "so that there is some code above and below
set signcolumn=yes:1
set hlsearch
set showcmd
set hidden
set visualbell "no sound
set mouse=a
set updatetime=300
set shortmess+=c
set timeout timeoutlen=1500
set colorcolumn=80
set updatetime=50

set shiftwidth=4
set tabstop=4
set expandtab

set clipboard=unnamedplus
" Allow backspacing over everything in insert mode
set backspace=indent,eol,start

" Store lots of :cmdline history
set history=500

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" THEMING
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Make things colorful

if (has("nvim"))
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif
if (has("termguicolors"))
    set termguicolors
endif

let g:ranger_replace_netrw = 1

syntax on
let g:airline_theme='gruvbox'
colorscheme gruvbox

filetype plugin on
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" REMAPS
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
xnoremap <leader>p "_dP
nnoremap <leader>y "+y
vnoremap <leader>y "+y
nnoremap <leader>Y gg"+yG
nnoremap <leader>d "_d
vnoremap <leader>d "_d

"remap save because muscle memory
noremap <silent> <C-S> :update<CR>
vnoremap <silent> <C-S> <C-C>:update<CR>
inoremap <silent> <C-S> <C-O>:update<CR>

let mapleader = ";"

nnoremap <leader>ff <cmd>lua require('telescope.builtin').find_files()<cr>

" make search & replace case-insensitive
let g:rg_command = 'rg --vimgrep -S'

nmap <leader>cc <plug>NERDCommenterToggle
vmap <leader>cc <plug>NERDCommenterToggle

fun! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
	call winrestview(l:save)
endfun

"format and lint on exit
augroup tenderribs
    autocmd!
    " on save for all file types:
    autocmd BufWritePre * :call TrimWhitespace()
    "autocmd BufWritePre * lua vim.lsp.buf.formatting_sync()
augroup END
